package com.relativity6.prediction;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Prediction {

    private static final Logger logger = Logger.getLogger(Prediction.class.getName());

    public static void main(String[] args) {

        // create the request body data
        var bodyData = "{" +
                "\"token\": \"YOUR_TOKEN_HERE\"," +
                "\"name\": \"Apple Inc.\"," +
                "\"address\": \"One Apple Park Way\"," +
                "\"state\": \"CA\"" +
                "}";

        // create the http client
        var client = HttpClient.newHttpClient();

        // create the http request
        var request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.usemarvin.ai/naics/naics/search/"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(bodyData))
                .build();

        try {

            // send request to API
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            // show response in console
            logger.log(Level.INFO, "http request response: {}", response);
        }catch (InterruptedException interruptedException){

            // interrupted current thread
            Thread.currentThread().interrupt();

            // show exception in console
            logger.log(Level.INFO, "error - execution thread was interrupted", interruptedException);
        }catch(Exception e){

            // show exception in console
            logger.log(Level.INFO, "error - an error occur fetching prediction", e);
        }
    }
}
